extern crate rocket;
extern crate tempdir;

extern crate file_server;

use std::fs::File;
use std::io::Write;
use std::path::Path;

use rocket::local::Client;
use rocket::local::LocalResponse;
use rocket::http::{Status, Method};
use rocket::response::Body;

/// A helper function to execute tests in a temporary directory.
/// `before` is called after the temporary directory has been created but before the request is
/// made.
/// `after` is called after the request has been made.
fn t<Before, After>(method: Method, path: &str, mut before: Before, mut after: After)
    where Before: FnMut(&Path),
          After: FnMut(LocalResponse, &Path)
{
    let tempdir = tempdir::TempDir::new("tests").unwrap();
    before(tempdir.path());
    let rocket = file_server::app(tempdir.path());
    let client = Client::new(rocket).expect("valid rocket");
    let request = client.req(method, path);
    after(request.dispatch(), tempdir.path());
}

#[test]
fn index() {
    t(Method::Get, "/", |_| {}, |mut response, _| {
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.body().and_then(|b| b.into_string()),
                   Some("OK\n".into()));
    });
}

#[test]
fn root_list() {
    t(Method::Get,
      "/?",
      |path| {
          File::create(path.join("foo.txt")).unwrap();
          File::create(path.join("bar.txt")).unwrap();
          // This file should not show up in the response.
          File::create(path.join(".baz")).unwrap();
      },
      |mut response, _| {
          assert_eq!(response.status(), Status::Ok);
          assert_eq!(response.body().and_then(|b| b.into_string()),
                     Some("bar.txt\nfoo.txt\n".into()));
      });
}

#[test]
fn files_head() {
    // File that doesn't exist.
    t(Method::Head, "/foo.txt", |_| {}, |mut response, _| {
        assert_eq!(response.status(), Status::NotFound);
        assert!(response.body().is_none());
    });

    // File that exists.
    t(Method::Head,
      "/foo.txt",
      |path| {
          let mut f = File::create(path.join("foo.txt")).unwrap();
          f.write_all(b"hello!").unwrap();
      },
      |mut response, _| {
        assert_eq!(response.status(), Status::Ok);
        match response.body() {
            Some(Body::Sized(_, 6)) => {} // OK,
            otherwise => panic!("body should be 6 bytes, found {:?}", otherwise),
        }
    });
}

#[test]
fn files() {
    // File that doesn't exist.
    t(Method::Get, "/foo.txt", |_| {}, |response, _| {
        assert_eq!(response.status(), Status::NotFound);
    });

    // File that exists.
    t(Method::Get,
      "/foo.txt",
      |path| {
          let mut f = File::create(path.join("foo.txt")).unwrap();
          f.write_all(b"hello!").unwrap();
      },
      |mut response, _| {
          assert_eq!(response.status(), Status::Ok);
          assert_eq!(response.body().and_then(|b| b.into_string()),
                     Some("hello!".into()));
      });
}

#[test]
fn actions() {
    // Directory should ignore actions.
    t(Method::Get,
      "/foo?clear",
      |path| {
          std::fs::create_dir(path.join("foo")).unwrap();
          File::create(path.join("foo").join("bar.txt")).unwrap();
      },
      |mut response, _| {
          assert_eq!(response.status(), Status::Ok);
          assert_eq!(response.body().and_then(|b| b.into_string()),
                     Some("bar.txt\n".into()));
      });

    // File
    // "clear" action should map to ".cleared"
    t(Method::Get,
      "/foo.txt?clear",
      |path| {
          let mut f = File::create(path.join("foo.txt")).unwrap();
          f.write_all(b"hello!").unwrap();
      },
      |mut response, path| {
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.body().and_then(|b| b.into_string()),
                   Some("OK\n".into()));
        assert!(!path.join(".clear").join("foo.txt").exists());
        assert!(path.join(".cleared").join("foo.txt").exists());
    });

    // other actions should just prepend "."
    t(Method::Get,
      "/foo.txt?delete",
      |path| {
          let mut f = File::create(path.join("foo.txt")).unwrap();
          f.write_all(b"hello!").unwrap();
      },
      |mut response, path| {
          assert_eq!(response.status(), Status::Ok);
          assert_eq!(response.body().and_then(|b| b.into_string()),
                     Some("OK\n".into()));
          assert!(path.join(".delete").join("foo.txt").exists());
      });
}

#[test]
fn upload() {
    let tempdir = tempdir::TempDir::new("tests").unwrap();
    let path = tempdir.path().join("foo/bar.txt");
    let rocket = file_server::app(tempdir.path());
    // The file shouldn't exist here because the request has not been made.
    assert!(!path.exists());

    let client = Client::new(rocket).expect("valid rocket");
    let request = client.req(Method::Put, "foo/bar.txt").body("Hello,\nWorld!");
    let mut response = request.dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body().and_then(|b| b.into_string()),
               Some("OK\n".into()));
    // The file should exist here because the request has been made.
    assert!(path.exists());
    // And we should be able to access the file now.
    let request = client.req(Method::Get, "foo/bar.txt");
    let mut response = request.dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(response.body().and_then(|b| b.into_string()),
               Some("Hello,\nWorld!".into()));
}
