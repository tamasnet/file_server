#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate file_server;

fn main() {
    file_server::app("files").launch();
}
