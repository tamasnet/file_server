#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;

use std::fs;
use std::io::{self, Cursor};
use std::path::{Path, PathBuf};

use rocket::http::{Status, ContentType};
use rocket::outcome::Outcome::*;
use rocket::request::{self, Request, FromRequest, FromForm, FormItems};
use rocket::response::{Body, NamedFile, Responder, Response};
use rocket::{Data, State};

#[derive(Debug)]
struct Config {
    /// The root directory from which the files will be served.
    root: PathBuf,
}

#[derive(Debug)]
struct FileAction(String);

impl<'a, 'r> FromRequest<'a, 'r> for FileAction {
    type Error = ();
    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, ()> {
        Success(FileAction(request.uri().query().unwrap().to_string()))
    }
}

struct Ignored;

impl<'f> FromForm<'f> for Ignored {
    type Error = ();
    fn from_form(items: &mut FormItems<'f>, strict: bool) -> Result<Self, Self::Error> {
        items.mark_complete();
        Ok(Ignored)
    }
}

enum CustomResponse {
    FileAction(PathBuf, FileAction),
    DirectoryListing(PathBuf),
    Error,
}

impl<'r> Responder<'r> for CustomResponse {
    fn respond_to(self, request: &Request) -> Result<Response<'r>, Status> {
        match self {
            CustomResponse::FileAction(_, _) => {
                Response::build()
                    .header(ContentType::Plain)
                    .sized_body(Cursor::new("OK\n"))
                    .ok()
            }
            CustomResponse::DirectoryListing(dir) => {
                Response::build()
                    .header(ContentType::Plain)
                    .sized_body(Cursor::new(get_file_names(&dir)))
                    .ok()
            }
            CustomResponse::Error => Err(Status::InternalServerError),
        }
    }
}

fn get_file_names(dir: &Path) -> String {
    let mut buffer = String::with_capacity(1024);
    for entry in fs::read_dir(dir).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        if !path.is_dir() {
            let name = path.file_name().unwrap().to_str().unwrap();
            if !name.starts_with(".") {
                buffer.push_str(name);
                buffer.push('\n');
            }
        }
    }
    buffer
}

#[get("/")]
fn index() -> &'static str {
    "OK\n"
}

#[get("/?<ignored>")]
fn root_list(ignored: Ignored, config: State<Config>) -> Option<CustomResponse> {
    Some(CustomResponse::DirectoryListing(config.root.clone()))
}

#[head("/<file..>")]
fn files_head(file: PathBuf, config: State<Config>) -> Response {
    let path = config.root.join(file);
    if path.exists() {
        let len = path.metadata().unwrap().len();
        let mut response = Response::new();
        response.set_raw_body(Body::Sized(Cursor::new(""), len));
        response
    } else {
        Response::build().status(Status::NotFound).finalize()
    }
}

#[get("/<file..>")]
fn files(file: PathBuf, config: State<Config>) -> Option<NamedFile> {
    let path = config.root.join(file);
    NamedFile::open(path).ok()
}

#[get("/<file..>?<ignored>")]
fn actions(file: PathBuf,
           ignored: Ignored,
           action: FileAction,
           config: State<Config>)
           -> Option<CustomResponse> {
    println!("performing action {:?} on {:?}", action, file.as_path());
    let path = config.root.join(file);
    let meta = path.metadata().unwrap();
    if meta.is_dir() {
        Some(CustomResponse::DirectoryListing(path))
    } else {
        match apply_status(&path, &action.0) {
            Ok(_) => Some(CustomResponse::FileAction(path, action)),
            Err(_) => Some(CustomResponse::Error),
        }
    }
}

fn apply_status(path: &Path, status: &str) -> io::Result<()> {
    let dir = path.parent().unwrap();
    let status_dir = match status {
        "clear" => dir.join(".cleared"),
        otherwise => dir.join(format!(".{}", otherwise)),
    };
    std::fs::create_dir_all(&status_dir)?;
    let new_path = status_dir.join(path.file_name().unwrap());
    std::fs::rename(path, new_path)
}

#[put("/<file..>", data = "<data>")]
fn upload(file: PathBuf, data: Data, config: State<Config>) -> io::Result<&'static str> {
    let path = config.root.join(file);
    let dir = path.parent().unwrap();
    std::fs::create_dir_all(&dir)?;
    println!("saving to {:?}", path);
    data.stream_to_file(&path)?;
    Ok("OK\n")
}

pub fn app<P: Into<PathBuf>>(root: P) -> rocket::Rocket {
    let config = Config { root: root.into() };
    rocket::ignite().manage(config).mount("/",
                                          routes![index, root_list, files_head, files, actions,
                                                  upload])
}
